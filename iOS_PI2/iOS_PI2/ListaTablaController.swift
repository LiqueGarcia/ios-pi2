//
//  ListaTablaController.swift
//  iOS_PI2
//
//  Created by Anderson on 31/05/18.
//  Copyright © 2018 andbie.inc. All rights reserved.
//

import UIKit

class ListaTablaController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension ListaTablaController:  UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 25
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let col = tableView.dequeueReusableCell(withIdentifier: "ListaTablaCell", for: indexPath) as! ListaTablaCell
        
        return col
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
         let height = (tableView.frame.height - 10 )/7
        
        return height
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        let vc = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ListBiciNavigController") as! ListBiciNavigController
    
        navigationController?.pushViewController(vc, animated: true)
        
    }
    
}
