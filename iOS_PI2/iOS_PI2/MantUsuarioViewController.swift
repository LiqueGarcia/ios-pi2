//
//  MantUsuarioViewController.swift
//  iOS_PI2
//
//  Created by Anderson on 4/06/18.
//  Copyright © 2018 andbie.inc. All rights reserved.
//

import UIKit

class MantUsuarioViewController: UIViewController {

    @IBOutlet weak var imgUsuario: UIImageView!
    @IBOutlet weak var myDatePicker: UIDatePicker!
    @IBAction func fechaNac(_ sender: Any) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        
        var strDate = dateFormatter.string(from: myDatePicker.date)
        
       // self.myDatePicker = strDate
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        imgUsuario.layer.cornerRadius = imgUsuario.bounds.size.width / 2.0
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
}
