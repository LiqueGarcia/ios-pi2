//
//  RegistrarseController.swift
//  iOS_PI2
//
//  Created by Anderson on 31/05/18.
//  Copyright © 2018 andbie.inc. All rights reserved.
//

import UIKit

class RegistrarseController: UIViewController {

    @IBOutlet weak var imgLogo: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        imgLogo.layer.cornerRadius = imgLogo.bounds.size.width / 2.0
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }

    

}
